// Теоретичні питання:
// 1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
// try...catch дозволяє перехоплювати помилки з коду, коли ми працюємо з об'єктами при запиті на сервер або обробляємо дані від користувача.

// Завдання:
// Дано масив books.
// 1. Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// 2. На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// 3. Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// 4. Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const validateObject = (obj, requiredKeys) => {
  for (const key of requiredKeys) {
    if (!(key in obj)) {
      throw new Error(`${JSON.stringify(obj)} не містить ключа ${key}`);
    }
  }
};

const createList = (obj) => {
  const listItems = Object.entries(obj).map(
    ([key, value]) => `<li>${key}: ${value}</li>`
  );
  return `<ul>${listItems.join("")}</ul>`;
};

const outputList = (array, requiredKeys) => {
  const root = document.querySelector("#root");
  for (const obj of array) {
    try {
      validateObject(obj, requiredKeys);
      const list = createList(obj);
      root.insertAdjacentHTML("beforeend", list);
    } catch (error) {
      console.log(error.message);
    }
  }
};

outputList(books, ["name", "author", "price"]);
